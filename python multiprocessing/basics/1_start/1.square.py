import os
import time
from multiprocessing import Process, current_process
start = 0



def square(number):
    result = number * number

    # U can use "os" module in python to print out Process ID 
    # assigment to the call of thi function assigment by operating system .
    process_id = os.getpid()
    #print(f"process ID : {process_id}")
    # U can also use "current process" funtion to get the name 
    # of the process object.
    process_name = current_process().name
    #print(f"Process name : {process_name}")

    print(f'the square of {number} is {result}')
    end = time.time()
    print(end - start)
    


if __name__ == '__main__':
    start = time.time()
    processes = []
    numbers = [1, 2, 3, 4]
    
    for num in numbers:

        process = Process(target=square, args=(num,))
        processes.append(process)

        # prossess are spawnd by creating a Process object and
        # then calling its start() method.
        process.start()
        
