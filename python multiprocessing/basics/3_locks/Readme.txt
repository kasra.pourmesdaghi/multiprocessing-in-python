1 :
in the first example, we are going to see conflicts
of access to the shared variable without multiprocessing
this is the base of what we need out of the program to see
as a result.

2 :
in the second program(3_no_locks__shared_value.py) we are going
to see what happens when the output of the program should
be 500 but if execute the program you might get a different
answer, this what is called the conflict in shared memory u
should have 500 as an answer in next program we are going
to fix this issue with locks.

3 :
we added locks to make sure the sequence occurs as it should
the way that we did it is after adding lock() function to args
of the program we lock function before equation part and releas
lock after and we do it for each function so every process occurs
sequentially.
the output should be 500


