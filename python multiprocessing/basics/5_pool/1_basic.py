import time
from random import randint
from multiprocessing import Pool


def sum_square(numbers):
    s = 0
    for i in range(numbers):
        s += i*i
    return s


def sum_square_mp(numbers):
    Start_time = time.time()
    # how many proccesors to give to program by defualt its maximum 
    p = Pool()
    result = p.map(sum_square, numbers)
    p.close()
    p.join
    end_time = time.time() - Start_time
    print(result)
    print(f"processing {len(numbers)} numbers took {end_time} time using MP")

def sum_square_no_mp(numbers):
    Start_time = time.time()
    result = []
    for i in numbers:
        result.append(sum_square(i))
    end_time = time.time() - Start_time
    print(result)
    print(f"processing {len(numbers)} numbers took {end_time} time using no MP(serial)")
    


if __name__ == '__main__':
    numbers = [randint(1, 100) for i in range(1, 1000000)]
    
    sum_square_mp(numbers)
    sum_square_no_mp(numbers)