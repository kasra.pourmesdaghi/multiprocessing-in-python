import time
import logging
from multiprocessing import Process, Lock, Value
from multiprocessing import  log_to_stderr, get_logger

def add_500_lock(total, lock):
    for i in range(100):
        time.sleep(0.01)
        lock.acquire()
        total.value += 5
        lock.release()


def sub_500_lock(total, lock):
    for i in range(100):
        time.sleep(5)
        lock.acquire()
        total.value -= 5
        print(total.value)
        lock.release()


if __name__ == '__main__':
    # shared resorce using .value method
    total = Value('i', 500)
    lock = Lock()

    
    #prints out all loging info as we run this code
    log_to_stderr()
    #creat object
    logger = get_logger()
    #with this function we set level of information that what we want to see 
    logger.setLevel(logging.INFO)


    # creat 2 process objects for each function
    add_process = Process(target=add_500_lock, args=(total, lock))
    sub_process = Process(target=sub_500_lock, args=(total, lock))

    add_process.start()
    sub_process.start()

    add_process.join()
    sub_process.join()

    print(total.value)
