import os
import time
from multiprocessing import Process, current_process,Manager


def square(numbers):

    for number in numbers:
        #sleep for one second to see processing in task manager
        time.sleep(0.5)  
        result = number * number
        print(f'the square of {number} is {result}')
        # process_id = os.getpid()
        # print(f"process ID : {process_id}")
        # print("---------------------------------------")


if __name__ == '__main__':

    processes = []
    numbers = range(100)

    for i in range(50):

        #we are passing list to this instance
        process = Process(target=square, args=(numbers,))
        processes.append(process)

        # prossess are spawnd by creating a Process object and
        # then calling its start() method.
        process.start()

    for process in processes:
        #to join all processes
        Process.join()

        
    print('program is done')