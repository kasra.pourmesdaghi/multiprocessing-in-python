import time
import os
import pysnooper
from random import randint
from multiprocessing import Pool
from multiprocessing import Process, Lock, Value

#global var
numbers = [randint(1, 100) for i in range(1, 100)]
isSorted = True
# @pysnooper.snoop()
# def compare(i):

#     if numbers[i] > numbers[i+1]:
#         numbers[i], numbers[i+1] = numbers[i+1], numbers[i]
#         isSorted = True
#     return isSorted

def odd_sort(numbers, lock):
    N = len(numbers)
    for i in range(1, N-1, 2):
        lock.acquire()
        if numbers[i] > numbers[i+1]: 
            numbers[i], numbers[i+1] = numbers[i+1], numbers[i] 
            global isSorted
            isSorted = True
        
        lock.release()



def even_sort(numbers, lock):
    N = len(numbers)
    for i in range(0, N-1, 2):
        lock.acquire()
        if numbers[i] > numbers[i+1]: 
            numbers[i], numbers[i+1] = numbers[i+1], numbers[i] 
            global isSorted
            isSorted = True
        
        lock.release()



def Multiprocessing(numbers):
    Start_time = time.time()
    
    #p = Pool(os.cpu_count())
    print('cpu count is : ' + str(os.cpu_count()))
    
    lock = Lock()
    global numbers
    global isSorted
    
    while isSorted == True:
        
        isSorted = False

        
        
        # creat 2 process objects for each function
        odd = Process(target=odd_sort, args=(numbers, lock))
        even = Process(target=even_sort, args=(numbers, lock))

        odd.start()
        even.start()

        odd.join()
        even.join()

        print(numbers)


    

    end_time = time.time() - Start_time
    print(f"processing {len(numbers)} numbers took {end_time} time using MP")


def serial(numbers):
    start = time.time()
    data = [randint(1, 100) for i in range(1, 10000000)]
    print(data)
    check_swap = ''
    Nt_of_list = len(data)
    # while chcheck_swap == True:
    for j in range(Nt_of_list - 1):
        # print('out :' + str(i))
        check_swap = False

        for i in range(Nt_of_list - 1):
            # print('in :' + str(i))

            if data[i] > data[i+1]:
                # midle var
                swap = data[i]
                data[i] = data[i+1]
                data[i+1] = swap
                check_swap = True
        print(j)
        if check_swap == False:
            break

    print(data)
    end = time.time()
    print('run time : ')
    print(end - start)


if __name__ == '__main__':
   
   
    # how many proccesors to give to program
    Multiprocessing(numbers)
    print(numbers)
    # serial(numbers)
